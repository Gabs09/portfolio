# Portfolio

### Hello, my name is Gabriel Savet

I am currently studying at CODERS.BAY Vienna as an application developer. Here are some of my projects I have worked on throughout my coding journey. 

## Projects

### [Chess Swap](https://gitlab.com/Gabs09/chessswap)
In this Project I got to create an App using Kotlin/Jetpack Compose.
Chess Swap is a game similar to a normal chess game but each player has the possibility to swap one of their own chess pieces(max. three times per game) during their turn. 

## Tech Stack


### WEB
|  | |
|---|---|
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg" width="30" height="30"> | HTML5 |
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg" width="30" height="30"> | CSS3 |
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg" width="30" height="30"> | Javascript |
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vuejs/vuejs-original.svg" width="30" height="30"> | VueJS |
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vuetify/vuetify-original.svg" width="30" height="30"> | Vuetify |
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-original.svg" width="30" height="30"> | Bootstrap |
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/laravel/laravel-plain-wordmark.svg" width="30" height="30"> | Laravel |

### JAVA
|  | |
|---|---|
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/java/java-original.svg" width="30" height="30"> | Java |
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/spring/spring-original.svg" width="30" height="30">| Spring Boot |

### KOTLIN
|  | |
|---|---|
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/kotlin/kotlin-original.svg" width="30" height="30"> | Kotlin |
|<img src="https://3.bp.blogspot.com/-VVp3WvJvl84/X0Vu6EjYqDI/AAAAAAAAPjU/ZOMKiUlgfg8ok8DY8Hc-ocOvGdB0z86AgCLcBGAsYHQ/s1600/jetpack%2Bcompose%2Bicon_RGB.png" width="30" height="30">| Jetpack Compose |

### DATABASE
|  | |
|---|---|
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-original-wordmark.svg" width="30" height="30"> | MySQL |
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/postgresql/postgresql-original-wordmark.svg" width="30" height="30">| PostgreSQL |

### VERSION CONTROL
|| |
|---|---|
|<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/gitlab/gitlab-original.svg" width="30" height="30">  | GitLab |

